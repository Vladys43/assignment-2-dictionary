ASM=nasm
ASMFLAGS=-f elf64
LD=ld

build: clean out/result

build/%.o: src/%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

out/result: build/main.o build/dict.o build/lib.o
	$(LD) -o out/result build/lib.o build/dict.o build/main.o

clean:
	rm build/*
	rm out/*

.PHONY: build clean
