%include "src/lib.inc"
%define NULL 0
%define KEY_OFFSET 8
global find_word

section .text

; rdi - str pointer
; rsi - dict pointer
; rax - match from dic 

find_word:
    xor rax, rax
    .loop:
        cmp rsi, NULL
        je .no_element
        push rdi
        push rsi
        add rsi, KEY_OFFSET
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .got_element
        mov rsi, [rsi]
        jmp .loop
    .got_element:
        mov rax, rsi
        ret
    .no_element:
        xor rax, rax 
        ret