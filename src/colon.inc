%define POINT 0

%macro colon 2
%%next: dq POINT
%ifstr %1
db %1,0
%else
    %error "first argument isn't string"
%endif
%ifid %2
%2:
%else
    %error "second argument not id"
%endif
%define POINT %%next
%endmacro