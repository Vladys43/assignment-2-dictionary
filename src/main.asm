%include "src/colon.inc"
%include "src/lib.inc"
%include "src/dict.inc"
%include "src/words.inc"
%define buffer_size 256
global _start

section .rodata

enter_message: db "Enter key: ", 0
not_found: db "Wrong key", 0
input_error: db "Input error", 0
print_message_key: db "Got key: ", 0
print_message_value: db "Got value: ", 0

section .bss

buffer: resb buffer_size

section .text

_start:
    mov rdi, enter_message
    call print_string

    .read:
        mov rdi, buffer
        mov rsi, buffer_size
        call read_line
        test rax, rax
        jz .fail_input
        push rdx ;key length
        mov rdi, buffer
    .check_dict:
        mov rdi, buffer
        mov rsi, POINT
        call find_word
        test rax, rax
        jz .no_match

    .print:
        ;rax on link
        mov rdi, print_message_key
        push rax
        call print_string
        pop rax
        add rax, 8 ; move to key
        mov rdi, rax
        push rdi
        call print_string ; print key
        call print_newline
        mov rdi, print_message_value
        call print_string
        pop rdi
        pop rdx
        lea rdi, [rdi + rdx + 1] ; to the start of input
        call print_string ; print value
        call print_newline
        jmp .exit
    .no_match:
        mov rdi, not_found
        call print_string
        jmp .exit
    .fail_input:
        mov rdi, input_error
        call print_string
        call exit_error
    .exit:
        call exit_ok
